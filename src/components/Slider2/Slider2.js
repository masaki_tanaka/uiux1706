import React, {Component, PropTypes} from 'react';
import InnerSlider from './InnerSlider';

export default class Slider2 extends Component {
  static propTypes = {
    children: PropTypes.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <InnerSlider>
        {this.props.children}
      </InnerSlider>
    );
  }
}
