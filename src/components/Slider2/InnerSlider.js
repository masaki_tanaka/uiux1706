import React, {Component, PropTypes} from 'react';
import Track from './Track';

export default class InnerSlider extends Component {
  static propTypes = {
    children: PropTypes.isRequired,
  };

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Track>
        {this.props.children}
      </Track>
    );
  }
}
