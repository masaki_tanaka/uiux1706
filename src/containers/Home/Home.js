import React, {Component} from 'react';
import Slider from 'react-slick';
import Slider2 from '../../components/Slider2/Slider2';

export default class Home extends Component {
  render() {
    const mainLeft = require('./main-lift.png');
    const mainRight = require('./main-right.png');
    const item = require('./item.png');
    const styles = require('./Home.scss');
    const settings = {
      dots: true,
      infinite: true,
      arrows: true,
      speed: 500,
      slidesToShow: 2,
      slidesToScroll: 1
    };

    return (
      <div>
        <main>
          <article>
            <div className={styles.mainContents}>
              <a href="#" className={styles.leftButton}>
                <span className={styles.leftArrow}></span>
              </a>
              <div className={styles.mainContentsBlock}>
                <img src={mainLeft}/>
              </div>
              <div className={styles.mainContentsBlock}>
                <ul>
                  <li><img src={mainRight}/></li>
                  <li><img src={mainRight}/></li>
                </ul>
                <ul>
                  <li><img src={mainRight}/></li>
                  <li><img src={mainRight}/></li>
                </ul>
              </div>
              <a href="#" className={styles.rightButton}>
                <span className={styles.rightArrow}></span>
              </a>
            </div>
            <div className={styles.newItems}>
              <h2>■ 新着商品</h2>
              <a href="#" className={styles.leftButton}>
                <span className={styles.leftArrow}></span>
              </a>
              <div className={styles.newItemsBlock}>
                <ul>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                </ul>
                <ul>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                </ul>
              </div>
              <div className={styles.rightButton}>
                <div className={styles.rightArrow}></div>
              </div>
              <p className={styles.otherNewItems}><span>その他の新商品一覧 ➤</span></p>
            </div>
            <div className={styles.recommendItems}>
              <h2>■ あなたへのおすすめ商品</h2>
              <div>
                <ul>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                  <li>
                    <div className={styles.panel}>
                      <h3>5月28日 受注開始</h3>
                      <img src={item}/>
                      <p>商品名が入ります</p>
                    </div>
                  </li>
                </ul>
              </div>
            </div>
            <div className={styles.shopList}>
              <h2>■ ショップ一覧</h2>
              <ul>
                <li>
                  <h3>カテゴリが入ります</h3>
                  <ul className={styles.subCategories}>
                    <li>サブカテゴリが入ります</li>
                    <li>サブカテゴリが入ります</li>
                  </ul>
                </li>
                <li>
                  <h3>カテゴリが入ります</h3>
                  <ul className={styles.subCategories}>
                    <li>サブカテゴリが入ります</li>
                    <li>サブカテゴリが入ります</li>
                  </ul>
                </li>
                <li>
                  <h3>カテゴリが入ります</h3>
                  <ul className={styles.subCategories}>
                    <li>サブカテゴリが入ります</li>
                    <li>サブカテゴリが入ります</li>
                  </ul>
                </li>
              </ul>
            </div>
            <div className={styles.news}>
              <h2>■ ニュースリリース</h2>
              <ul>
                <li><span className={styles.date}>2017年5月26日</span><span
                  className={styles.headline}>あああああああああああああああああああああああああああ</span>
                </li>
                <li><span className={styles.date}>2017年5月25日</span><span
                  className={styles.headline}>あああああああああああああああああああああああああああ</span>
                </li>
                <li><span className={styles.date}>2017年5月24日</span><span
                  className={styles.headline}>ああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああああ</span>
                </li>
                <li><span className={styles.date}>2017年5月23日</span><span
                  className={styles.headline}>あああああああああああああああああああああああああああ</span>
                </li>
              </ul>
            </div>
            <div className={styles.carousel}>
              <Slider {...settings}>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3>4</h3></div>
                <div><h3>5</h3></div>
                <div><h3>6</h3></div>
              </Slider>
            </div>
            <div className={styles.carousel2}>
              <Slider2>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3><img src={mainLeft}/></h3></div>
                <div><h3>4</h3></div>
                <div><h3>5</h3></div>
                <div><h3>6</h3></div>
              </Slider2>
            </div>
          </article>
        </main>
      </div>
    );
  }
}
