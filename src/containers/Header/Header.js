import React, {Component} from 'react';
import {Nav} from 'containers';

export default class Header extends Component {
  render() {
    const styles = require('./Header.scss');
    const banner = require('./top-banner.png');

    return (
      <div className={styles.header}>
        <header>
          <div className={styles.banner}><img src={banner}/></div>
          <div className={styles.title}>
            <h1>こどもから大人まで楽しめるxxxxx公式ショッピングサイト！</h1>
          </div>
          <Nav/>
        </header>
      </div>
    );
  }
}
