import React, {Component} from 'react';

export default class Nav extends Component {
  render() {
    const logo = require('./site-logo.png');
    const styles = require('./Nav.scss');

    return (
      <div className={styles.nav}>
        <nav>
          <div className={styles.upperNav}>
            <ul className={styles.upperLeftNav}>
              <li><img src={logo}/></li>
              <li>
                <p>初めての方へ</p>
                <p>ご利用ガイド</p>
              </li>
              <li>
                <ul className={styles.siteNav}>
                  <li>よくあるご質問</li>
                  <li>閲覧履歴</li>
                  <li>公式アプリ</li>
                </ul>
              </li>
              <li className={styles.greetingUser}>こんにちはゲストさん！</li>
            </ul>
            <ul className={styles.upperRightNav}>
              <li className={styles.login}>ログイン</li>
              <li className={styles.singUp}>会員登録</li>
              <li className={styles.cart}>カート</li>
            </ul>
          </div>
          <div className={styles.lowerNav}>
            <ul className={styles.lowerLeftNav}>
              <li>■ <span className={styles.searchWord}>キャラクターから探す</span></li>
              <li>■ <span className={styles.searchWord}>ショップから探す</span></li>
              <li>■ <span className={styles.searchWord}>商品ジャンルから探す</span></li>
              <li>■ <span className={styles.searchWord}>特集から探す</span></li>
              <li>■ <span className={styles.searchWord}>ポイントを貯める</span></li>
            </ul>

            <form className={styles.lowerRightNav}>
              <input type="text" name="search"/>
              <input type="submit" value="検索"/>
            </form>
          </div>
          <ul className={styles.information}>
            <li>配送に関するご連絡</li>
            <li>携帯電話での接続に関するご連絡</li>
          </ul>
        </nav>
      </div>
    );
  }
}
