export App from './App/App';
export Home from './Home/Home';
export NotFound from './NotFound/NotFound';
export Header from './Header/Header';
export Nav from './Nav/Nav';
export Footer from './Footer/Footer';
