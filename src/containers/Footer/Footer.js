import React, {Component} from 'react';

export default class Footer extends Component {
  render() {
    const styles = require('./Footer.scss');
    const corporate = require('./footer-corporate.png');

    return (
      <div>
        <footer>
          <div className={styles.line}></div>
          <div className={styles.corporate}>
            <img src={corporate}/>
            <div className={styles.footerTitle}>
              <p> こどもから大人まで楽しめる</p>
              <p>xxxx公式ショッピングサイト!!</p>
            </div>
            <p><a href="#">>トップに戻る</a></p>
            <p className={styles.copyright}>@会社名が入ります</p>
          </div>
          <ul className={styles.links}>
            <li>
              <h2>会社概要</h2>
              <ul>
                <li>会社概要</li>
                <li>ウェブサイトご利用条件</li>
                <li>個人情報保護方針</li>
                <li>特定商取引法の表示</li>
                <li>著作権</li>
                <li>採用情報</li>
              </ul>
            </li>
            <li>
              <h2>会社サービス</h2>
              <ul>
                <li>メルマガ会員登録</li>
                <li>会員規約</li>
              </ul>
            </li>
            <li>
              <h2>ガイド</h2>
              <ul>
                <li>よくあるご質問/ご利用</li>
                <li>ガイド</li>
                <li>安全ガイド</li>
                <li>営業日カレンダーについて</li>
                <li>お問い合わせ</li>
                <li>サイトマップ</li>
              </ul>
            </li>
          </ul>
        </footer>
      </div>
    );
  }
}
